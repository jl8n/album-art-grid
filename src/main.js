import App from './js/components/App.js';

new Vue({
  render: h => h(App),
}).$mount(`#app`);