import requests
import os
import json
import time
import jsbeautifier


with open('lastfm_config.json', 'r') as fin:
    data = json.load(fin)
    api_key = data['api_key']
    user_agent = data['user_agent']
    user = data['user']

url = 'http://ws.audioscrobbler.com/2.0/'
headers = {
    'user-agent': user_agent
}
payload = {
    'api_key':  api_key,
    'user': user,
    'method': 'user.getRecentTracks',
    'format': 'json',
    'limit': 200,  # max 200, amount of json objects per request
    'page': None 
}

response = requests.get(url, headers=headers, params=payload)
totalPages = int(response.json()['recenttracks']['@attr']['totalPages'])

json_file = 'scrobbles.json'
with open(json_file, 'w') as fout:
    for i in range(1, 2):
        print("page {}/{}".format(i, totalPages))

        payload['page'] = i
        response = requests.get(url, headers=headers, params=payload)
        data = response.json()
        json.dump(data, fout)  # write to file
        time.sleep(0.75)  # rate limiter
    

