const fs = require('fs');
const request = require('request');

console.log("runnning...");


var download = function(uri, filename, callback){
  request.head(uri, function(err, res, body){
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);

    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};


fs.readFile('spotify-listens.json', 'utf8', (err, fileContents) => {
  if (err) {
    console.error(err);
    return;
  }
  try {
    const data = JSON.parse(fileContents);
    let keys = Object.entries(data.items);

    for (const key of keys) {
      let album = key[1].track.album.name;
      let artist = key[1].track.artists[0].name;
      let track = key[1].track.name;
      let img = key[1].track.album.images[0].url;
      console.log(img);
      download(img, `../img/album-art${artist} - ${album}.jpg`);
      //console.log(artist + " - " + album + " - " + track);
    }
  } catch(err) {
    console.error(err);
  }
});


