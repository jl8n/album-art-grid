import requests
import json
import time
import os
from os.path import isfile, join
import urllib.parse
import unicodedata


def json_format(jstring):
    jstring = json.dumps(jstring, sort_keys=True, indent=4)
    return jstring


def get_mbid(artist, album):
    url_artist = urllib.parse.quote(artist)
    url_album = urllib.parse.quote(album)
    url = 'http://musicbrainz.org/ws/2/release/?query=artist:{}+release:{}&fmt=json'.format(url_artist, url_album)
    response = requests.get(url)
    data = response.json()

    artist = unicodedata.normalize('NFD', artist)\
        .encode('ascii', 'ignore')\
        .decode("utf-8").lower()
    album = unicodedata.normalize('NFD', album)\
        .encode('ascii', 'ignore')\
        .decode("utf-8").lower()

    print('{} - {} ...searching'.format(artist, album))
    for line in data['releases']:  # loop over search results
        found_album = line['title']
        found_artist = line['artist-credit'][0]['artist']['name']

        # make strings uniform for better searches

        found_artist = unicodedata.normalize('NFD', found_artist)\
           .encode('ascii', 'ignore')\
           .decode("utf-8").lower()
        found_album = unicodedata.normalize('NFD', found_album)\
           .encode('ascii', 'ignore')\
           .decode("utf-8").lower()

        print('{} - {} ...found'.format(found_artist, found_album))

        if album == found_album:
            print("match", album, artist)
            return found_artist, line['id'] 
        #if album == found_album and artist == found_artist:




    #print(json_format(data['releases']))
    #print(artist, album, data)

#print(get_mbid("The Fall of Troy", "Doppleganger"))
#print(get_mbid("Erra", "Augment"))


with open('scrobbles.json', 'r') as fin:
    data = json.load(fin)

    for track in data['recenttracks']['track']:
        name = track['name']
        artist = track['artist']['#text']
        album = track ['album']['#text']
        #album_mbid = track['album']['mbid']



        artist, album_mbid = get_mbid(artist, album)

        os.chdir('../img')
        dir_files = [f for f in os.listdir('.')]
        dir_files.append('.')  # current dir, loop at least once

        for file in dir_files:
            jpg_filename = artist + ' - ' + album + '.jpg'
            png_filename = artist + ' - ' + album + '.png'
            print(file, jpg_filename)

            if file == jpg_filename or file == png_filename:
                print(file + " matches")
                break
        
        print(file + " not found, downloading")
        url = 'https://coverartarchive.org/release/{}/front'.format(urllib.parse.quote(album_mbid))
        print(url)
        image_file = requests.get(url)
        fin_name, fin_ext = os.path.splitext(image_file.url)
        #fin_url = urllib.parse.urlencode(fin_name, fin_ext)
        fout_name = '{} - {}{}'.format(artist, album, fin_ext)

        with open(fout_name, 'wb') as fout:
            fout.write(image_file.content)

            
        print()



            #print(fout_name)
            
        #print(dir_files)