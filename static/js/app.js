/* ========== API calls ========== */
let getElementsFromServer = () => {
  return fetch('http://45.63.50.77:3000/albums');
};


let createAlbumOnServer = (album, artist) => {
  let data = `album=${encodeURIComponent(album)}`;
  data += `&artist=${encodeURIComponent(artist)}`;

  return fetch('http://45.63.50.77:3000/albums', {
    body: data,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }  
  });
};


let editAlbumOnServer = (metadata) => {
  let id = metadata.id;
  let name = metadata.name;
  let artist = metadata.artist;
  let release = metadata.release;
  let coverUrl = metadata.coverUrl;

  let data = `&name=${encodeURIComponent(name)}`;
  data += `&artist=${encodeURIComponent(artist)}`;
  data += `&release=${encodeURIComponent(release)}`;
  data += `&coverUrl=${encodeURIComponent(coverUrl)}`;

  return fetch(`http://45.63.50.77:3000/albums/${id}`, {
    body: data,
    method: 'PUT',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }    
  });
};


let deleteAlbumFromServer = (id) => {
  return fetch(`http://45.63.50.77:3000/albums/${id}`, {
    method: "DELETE"
  });
};


/* ========== Vue ========== */
var app = new Vue({
  el: "main",
  data: {
    // temp
    isActive: true,
    hasError: false,
    albumsObj: [],

    /*    v-if    */
    overlayHidden: true,
    showEdit: false,


    classObject: {
      active: true,
      'text-danger': false
    },

    toastError: false,  // styles the toast as an error
    toastHidden: false,
    toastMsg: '',
    menuWidth: '250px',

    /*    edit form    */
    editId: '',
    editName: '',
    editArtist: '',
    editRelease: '',
    editCover: '',



    profileName: 'Josh Layton',

    newAlbumName: "",
    newAlbumPath: "",
    newAlbumArtist: "",
    newAlbumDate: "",
    albums: [],

    activeTabIndex: '',
    closeTabIndex: '',

    albumName: "",
    artistName: "",
    newElementSymbol: "",
    newElementNumber: "",
    elements: []
  },

  computed: {
    computedNavWidth: function () {
      return this.menuWidth;
    }
  },

  methods: {

    hideToast: function () {
      this.toastHidden = true;
      setTimeout( () => {
        this.toastHidden = false;
      }, 4500);
    },

    albumClicked: function(event, index) {
      this.showEdit = true;
      this.editId = this.albumsObj[index]._id;
      this.editName =  this.albumsObj[index].name;
      this.editArtist =  this.albumsObj[index].artist;
      this.editRelease = this.albumsObj[index].release;
      this.editCover = this.albumsObj[index].coverUrl;
    },

    searchBtnClicked: function () {
      console.log("submit triggered");
      createAlbumOnServer(
        this.albumName,
        this.artistName
        )
        .then(response => {
          response.json();
          if (response.status == 404) {
            this.toastError = true;
            this.toastMsg = 'Error! We couldn\'t find that one.';
            this.hideToast();
          } else {
            this.showAlbums();
          }
        });

      //getElementsFromServer();
      //this.showAlbums();
    },

    delBtnClicked: function () {
      deleteAlbumFromServer(this.editId).then(response => {
        console.log('Deleted', this.editId);
        this.showAlbums();
        this.editId = this.editName = this.editArtist = this.editRelease = this.editCover = '';
      });
    },

    editBtnClicked: function () {
      let metadata = {
        'id': this.editId,
        'name': this.editName,
        'artist': this.editArtist,
        'release': this.editRelease,
        'coverUrl': this.editCover
      };

      editAlbumOnServer(metadata).then(response => {
        if (response.status == 404) {
          this.toastError = true;
          this.toastMsg = 'That ID wasn\'t found';
          this.hideToast();
        } else {
          this.showAlbums();
        }
      });
    },

    showAlbums: function () {
      this.albums = [];  //  reset
      this.albumsObj = [];  //  reset
      getElementsFromServer().then(response => {
        response.json().then(albums => {;
          albums.forEach(album => {
            albumUrl = encodeURIComponent(album.coverUrl);
            this.albums.push('<img src=img/album-art/' + albumUrl + '>');
            this.albumsObj.push(album);
          });


        });
      });
    }
  },
  created: function () {
    console.log("VUE LOADED.");
    this.showAlbums();
  }
});


var app = new Vue({
  el: "footer",
  data: {
    current_year: new Date().getFullYear()
  }
});


/* ========== Helpers ========== */
let openNav = () => {
  document.getElementById("mySidenav").style.width = "250px";
};


let closeNav = () => {
  document.getElementById("mySidenav").style.width = "0";
};


let shuffle = (a) => {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
};