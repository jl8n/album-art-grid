#!/usr/bin/env node

const express = require('express');
const bodyParser = require('body-parser');
const fetch = require('node-fetch');  // API requests
const mongoose = require('mongoose');  // MongoDB
const fs = require('fs');  // file I/O
const fsp = require('fs').promises;
const util = require('util');
const cors = require('cors'); 

/* ========== Config ========== */
const app = express();
const port = 3000;
const readdir = util.promisify(fs.readdir);



/* ========== Globals ========== */
let artUrls = [];
const artDir = './static/img/album-art/';


/* ========== Middleware ========== */
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cors());


/* ========== MongoDB ========== */
const connectUrl = 'mongodb+srv://admin:LLSi1kL98TBCarQc@cluster0-pif7h.mongodb.net/midterm?retryWrites=true&w=majority';
mongoose.connect(connectUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.set('useFindAndModify', false);
var Album = mongoose.model('albums', {
  name: String,
  artist: String,
  release: String,
  coverUrl: String
});


/* ========== Helper Functions ========== */
const downloadFile = async (url, path) => {
  // TODO check if file exists first
  console.log("downloading file", url, "to", path);
  const res = await fetch(url);
  const fileStream = fs.createWriteStream(path);
  await new Promise((resolve, reject) => {
      res.body.pipe(fileStream);
      res.body.on("error", err => {
        reject(err);
      });
      fileStream.on("finish", () => {
        resolve();
      });
    });
};


const updateArtUrls = async () => {

  artUrls = [];

  fs.readdir(artDir, (err, files) => {
    files.forEach(file => {
      if (file.split('.').pop() == 'jpg' || file.split('.').pop() == 'png') {
        artUrls.push(file);
      }
    });
  });
};


/* ========== Promises ========== */
const getAlbumInfo = async (album, artist) => {
  let hostname = 'http://www.musicbrainz.org';
  let path = `/ws/2/release/?query=release:${album}%20AND%20artist:${artist}&fmt=json`;
  console.log(hostname + path);
  let res = await fetch(hostname + path, {
    headers: {
      'User-Agent': 'album-art-grid/1.0 ( josh.l8n@gmail.com )'
    }
  });

  return res.json();
};


const getAlbumArt = async (albumMbid) => {
  let hostname = 'http://www.coverartarchive.org';
  let path = `/release/${albumMbid}`;
  let res = await fetch(hostname + path, {
    headers: {
      'User-Agent': 'album-art-grid/1.0 ( josh.l8n@gmail.com )'
    }
  });

  return res.json();
};

/* ========== API ========== */
app.get('/', (req, res) => res.send('Hello World!'));


app.get('/albums', async (req, res) => {
  console.log('GET albums2');
  let files = await fsp.readdir('./static/img/album-art/');  // get all filenames from dir
  let filesSet = new Set(files);
  let albums = await Album.find();  // get all doc's from db
  let matches = [];

  albums.forEach(album => {
    if (filesSet.has(album.coverUrl)) {
      matches.push(album);
    }
  });

  return res.json(matches);
});


app.post('/albums', async (req, res) => {
  let album = encodeURI(req.body.album);
  let artist = encodeURI(req.body.artist);

  try {
    res1 = await getAlbumInfo(album, artist);

    album = res1.releases[0].title;
    artist = res1.releases[0]['artist-credit'][0].artist.name;
    let albumMbid = res1.releases[0].id;
    let artistMbid = res1.releases[0]['artist-credit'][0].artist.id;
    let date = res1.releases[0].date;
    const res2 = await getAlbumArt(albumMbid);
    const artUrl = res2.images[0].image;
    // check if album art exists before downloading
    //let fileName = artist + ' - ' + album;

    // save album art locally
    const fileExt = `${artUrl.split('.').pop()}`;
    const saveDir = 'static/img/album-art/';
    const foutPath = `${saveDir}${artist} - ${album}.${fileExt}`;
    console.log(foutPath);
    //let test = await fsp.access(foutPath);
    //console.log("Test", test);

    await downloadFile(artUrl, foutPath).catch(e => console.error(e.message));

    let dbAlbum = new Album({  // create an instance of the mongoose model
      name: album,
      artist: artist,
      release: date,
      coverUrl: `${artist} - ${album}.${fileExt}`,
    });
    dbAlbum.save();  // insert into the mongoose model
    console.log('inserted record into DB');

    let artUrls = await fsp.readdir('./static/img/album-art/');
    console.log(artUrls);
    res.json(artUrls);
  } catch(err) {
    res.sendStatus(404);
  }
});


app.put('/albums/:id', async (req, res) => {
  console.log(req.params.id);
  console.log('PUT /albums');
  let id = req.params.id;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    res.sendStatus(404);
  }
  let name = req.body.name;
  let artist = req.body.artist;
  let release = req.body.release;
  let coverUrl = req.body.coverUrl;

  await Album.findByIdAndUpdate({ _id: id }, {
    'name': name,
    'artist': artist,
    'release': release,
    'coverUrl': coverUrl
  }).catch(() => { return res.sendStatus(422); });

  return res.sendStatus(200);
});


app.delete('/albums/:id', (req, res) => {
  console.log("DELETE /albums/", req.params.id);
  Album.deleteOne({ _id: req.params.id }, (err) => {
    if (!err) {
      return res.sendStatus(200);
    }

    return res.sendStatus();
  });
});


app.listen(port, () => {
  console.log(`Listening on port ${port}!`);
});








  /*
  albumCallback = function(resp) {
    resp.setEncoding("utf8");
    let body = "";

    resp.on("data", data => {
      body += data;
    });
    resp.on("end", () => {
      body = JSON.parse(body);
      let releases = [];  // store MBID's of all found album releases

      try {
        let albumMbids = body['release-groups'][0].releases;
        albumMbids.forEach(item => {
          releases.push(item.id);
        });
      } catch (error) {
        return;
      }

      let albumInfo = {
        releases: releases,
        //albumMbid: body['release-groups'][0].releases[0].id,
        albumMbid: body['release-groups'][0].id,
        artistMbid: body['release-groups'][0]['artist-credit'][0].artist.id,
        album: body['release-groups'][0].title,
        artist: body['release-groups'][0]['artist-credit'][0].artist.name
      };


       * responds with the album art url
       * using the album art mbid found in the
       * first callback

      albumInfo.releases.forEach(release => {
        let options = {
          hostname: 'coverartarchive.org',
          path: `/release/${release}`,
          headers: {
            'User-Agent': 'MidtermAssignment/1.0 ( josh.l8n@gmail.com )'
          }
        };

        let albumArtCallback = function(resp) {
          resp.setEncoding("utf8");
          let body = "";

          resp.on("data", data => { body += data; });
          resp.on("end", () => {
            try {
              body = JSON.parse(body);
              coverArtUrl = body.images[0].thumbnails.large;
              console.log("url:", coverArtUrl);

              const fileExt = `${coverArtUrl.split('.').pop()}`;
              const foutPath = `static/img/album-art/${albumInfo.artist}-${albumInfo.album}.${fileExt}`;

              if (!fs.existsSync(foutPath)) {
                const file = fs.createWriteStream(foutPath);

                let artReq = http.get(coverArtUrl, function(response) {
                  console.log("writing album-art to file");
                  response.pipe(file);  // write album-art to file

                  const dir = './static/img/album-art/';
                  let albumCovers = [];
                
                  fs.readdir(dir, (err, files) => {
                    files.forEach(file => {
                      if (file.split('.').pop() == 'jpg' || file.split('.').pop() == 'png') {
                        albumCovers.push(file);
                      }
                    });
                
                    res.json(albumCovers);
                  });
                });
                console.log("ending");
                artReq.end();
              }
            } catch (error) {
              return;
            }
          });
        };

        http.get(options, albumArtCallback);
      });
    });
  };



  http.get(options, albumCallback);
    
  
  
  
  
  
  app.get('/albums', (req, res) => {
  // respond with list of all stored album-art
  const artDir = './static/img/album-art/';
  let albumCovers = [];
  fs.readdir(artDir, (err, files) => {
    files.forEach(file => {
      if (file.split('.').pop() == 'jpg' || file.split('.').pop() == 'png') {
        albumCovers.push(file);
      }
    });

    res.json(albumCovers);
  });
});
  
  
  */
