# album-art-grid

### Description

A web app for searching for and displaying album art in a collage

### Dependencies 

*  Vue.js
*  Node.js
    *  Express
    *  http
    *  fs
    *  path
    *  mongoose (MongoDB)


### Installation

`npm install`

### Start server

`node index.js`

